package cork;

import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner;
        MovieData md = new MovieData();
		boolean timeToQuit;

		System.out.print("MOVIE INFO");
	    do{
	        timeToQuit = true;
			scanner = new Scanner(System.in);

	        System.out.println();
	        System.out.print("Enter the name of a movie to search for (Enter to quit): ");
	        String searchValue = scanner.nextLine();
	        if (searchValue.length() > 0){

	        	timeToQuit = false;
                SearchResults results = md.getMovies(searchValue);
                if (results.getMovies().size() > 0){
                    results.displayResults();

                    System.out.println();
                    System.out.print("Enter the number of the movie for more info (-1 to quit): ");
                    int movieNumber = scanner.nextInt();
                    if (movieNumber >= 0) {

                        if (movieNumber < results.getMovies().size()){
                            Movie m = results.getMovie(movieNumber);
                            m.displayInfo();
                         } else {
                            System.out.println("Value not found");
                        }
                        System.out.println();
                    } else {
                        timeToQuit = true;
                    }
                } else {
                    System.out.println("No results found!");
                }
            }
       	} while (!timeToQuit);
	    System.out.println("done - thank you!");
    }
}
