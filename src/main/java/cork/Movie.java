package cork;

public class Movie {

    private String Title;
    private String Year;
    private String imdbID;
    private String Type;
    private String Poster;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        this.Title = title;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        this.Year = year;
    }

    public String getIMDBID() {
        return imdbID;
    }

    public void setIMDBID(String imdbID) {
        this.imdbID = imdbID;
    }
    public String getType() {
        return Type;
    }

    public void setType(String type) {
        this.Type = type;
    }

    public String getPoster() {
        return Poster;
    }

    public void setPoster(String poster) {
        this.Poster = poster;
    }

    public void displayInfo(){
        System.out.println("* Movie title = " + getTitle());
        System.out.println("* Movie year = " + getYear());
        System.out.println("* Movie imdbid = " + getIMDBID());
        System.out.println("* Movie poster = " + getPoster());
    }
}
