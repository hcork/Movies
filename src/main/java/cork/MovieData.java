package cork;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;


public class MovieData {

    final private String URL = "http://www.omdbapi.com/?";
    final private String CHARSET = "UTF-8";
    final private String KEY = "19daf1c7";
    final private int BUFFER_SIZE = 1024;
    final private char[] BUFFER = new char[BUFFER_SIZE];

    public SearchResults getMovies(String movieName){

        SearchResults sr = new SearchResults();
        Gson gson = new Gson();
        try{
            String query = String.format("s=%s&apikey=%s", URLEncoder.encode(movieName, CHARSET),  URLEncoder.encode(KEY, CHARSET));
            URLConnection connection = new URL(URL + query).openConnection();
            connection.setRequestProperty("Accept-Charset", CHARSET);
            InputStream response = connection.getInputStream();
            Reader in = new InputStreamReader(response, CHARSET);
            StringBuilder result = new StringBuilder();
            int read;
            while ((read = in.read(BUFFER, 0, BUFFER.length)) != -1) {
                result.append(BUFFER, 0, read);
            }
            sr = gson.fromJson(result.toString(), SearchResults.class);
        } catch (IOException ioe){
            System.out.println("Cannot get input stream");
        } catch (Exception e){
            System.out.print("exception: " + e.getMessage());
        }
        return sr;
    }

}
