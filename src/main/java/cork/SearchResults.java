package cork;

import java.util.ArrayList;
import java.util.List;

public class SearchResults {

    private List<Movie> Search = new ArrayList<Movie>();

    public List<Movie> getMovies() {
        return Search;
    }

    public void setMovies(List<Movie> movies) {
        this.Search = movies;
    }

    public Movie getMovie(int num){
        return Search.get(num);
    }

    public void displayResults(){
        int count = 0;
        for (Movie mv: Search){
            System.out.println(count++ + ": " + mv.getTitle());
        }

    }



}
